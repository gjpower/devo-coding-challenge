# Technical Assignment

The solutions to this assignment are separated into 2 projects
- "technical_test" includes solutions for the palindrome check
algorithm and the k-complementary pairs algorithm
- "TfIdf" includes solution for the Tf/Idf service

For both projects the build system uses gradle and the gradle
wrapper is provided. To initialise the gradle wrapper call
`./gradlew` from the project directory, this will download and
prepare the gradle wrapper as necessary. If gradle is already
installed on your machine it may be used instead.

The java jdk and jvm must be already installed on your machine
for compilation and running of the projects.

Depending on the configuration of your machine and JVM any UTF-8
tests may fail under Windows. A normal configuration using a
Linux distribution should work out-of-the-box.

JUnit5 is used for the test suite and will be fetched as a test
dependency when running `./gradlew test`

## "technical_test" project

### Palindrome

The implementation for palindrome is found in
`src/main/java/me/gjpower/Palindrome.java`
The solution found has a time complexity of O(n).
No assignments are made so space complexity is O(1), constant complexity.
This implementation determines that a single letter or empty string
cannot be a palindrome so will return false in such a case.
It will check the first half of elements against each of their
reflections and return true if they all match. It is unnecessary
to iterate past the half way point as this will just result in
double-checking.

### K-Complementary

The implementation for k-complementary is found in
`src/main/java/me/gjpower/KComplementary.java`
Two solutions are provided.

The rudimentary brute force implementation
`KComplementary.brute` will test each element against all
proceeding elements, this has a time complexity of O(n^2) and space complexity
of O(1) as no allocations are necessary.

The optimised implementation
`KComplementary.optimised` will first traverse the list and create a
hashmap for the value of each element to the indexes where that
value appears.
It then iterates through each key in the hashmap which is subtracted
from K in order to find the target pair. If that target is found in the
map the pairs, with all combinations of indexes where they appear and including
reflections (that is pair (j, i) as well as (i, j)), are added to the returned
list of matched pairs. The target is added to a set of already used values
that may be skipped.
The solution has a time complexity of O(n) and space complexity of O(n) as the
hashmap size is linearly proportional to the input search array size.
It may be slower than the brute solution
when the search array has very few elements, due to the necessity to traverse
the array first to build the hashmap and time spent doing key lookups when
traversing the constructed hashmap.

- To run the test suite call `./gradlew test`
- To run the palindrome example and k-complementary performance example call
`./gradlew run`

## "TfIdf" project

This solution is constructed of a few main parts.

`Document.java` reads, parses
and stores the input file, both as a Map providing the count of occurrences of
distinct terms for fast lookup as well as a full, in-memory string if the user
wishes to do an exhaustive search of the exact term (necessary if searching for
a short phrase as opposed to a single word). The document can then provide term
frequencies from either this hashmap or by the full search. This has a time
complexity O(n) and space complexity O(n) for the string cache and O(k) for
the term lookup map where k is the number of distinct terms in the text.
Frequency lookup time complexity will be O(k) for hashmap lookups and O(k\*n)
if full search is used where k is the number of terms to be looked up and
n is the text size.

`TfIdf.java` provides cached storage of term frequencies for a single term in a
list of documents. With these cached term frequencies IDF and then Tf/Idf may
then be calculated. Time and space complexity for these will scale O(n) on the
number of documents.

`ArgumentParser.java` provides a very basic argument parser for the purpose of this
project. I would prefer to use a well-supported argument parser or to develop a
fully featured one, but for the purposes of this test creating this basic
implementation is sufficient. It scans the arguments for flags
(arguments beginning with '-' and maps them to their proceeding values).
Time and space complexity O(n) for the number of arguments

`StringDoublePair` provides a basic pairing struct for the purposes of sorting
files by their Tf/Idf. This provides the ranking of the Tf/Idfs printed. In
this case it was used to sort a list but could alternatively be used for
sorting into a binary search tree.


All terms are converted to their lowercase in order to ignore capitalisation.
The solution provides a periodically running main loop that polls the directory
for the list of files, if any new files are detected they are subsequently parsed
and the Tf/Idfs for all terms are recalculated then summed and printed to give a
ranking of the total multi-term Tf/Idfs of requested terms for each document.

For the purpose of example a copyright free corpus is provided in `docs`

- To run the test suite call `./gradlew test`
- To run with terms likely found in "moby dick" run `./gradlew runWithWhaleTerms`
- To run with terms likely found in "pride and prejudice" run `./gradlew runWithPrideAndPrejudiceTerms`
- To run with terms likely found in Plato's "Republic" in Greek run `./gradlew runWithGreekTerms`
- To generate a local distribution to be found in `build/install/TfIdf/bin` run `./gradlew installDist`
with this your own arguments and files may be used
