package me.gjpower.tfidf;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import org.junit.jupiter.api.Test;

public class DocumentTests {

  @Test
  void checkTermFrequency() throws IOException {
    BufferedReader testSource =
        new BufferedReader(new StringReader("this this this this flies as a test"));

    Document testDocument = new Document(testSource, "test_Filename.txt");
    assertEquals(0.5, testDocument.termFrequency("this"));
    assertEquals(0.125, testDocument.termFrequencyFullSearch("flies"));
    assertEquals(testDocument.termFrequency("this"), testDocument.termFrequencyFullSearch("this"));
  }

  @Test
  void checkUTF8TermFrequency() throws IOException {
    BufferedReader testSource =
        new BufferedReader(new StringReader("Η Η θεμελιώδης βάσις του του του του Πλάτωνος είναι"));

    Document testDocument = new Document(testSource, "test_Filename.txt");
    System.out.println("Test");
    System.out.println(testDocument.termFrequency("του".toLowerCase()));
    assertEquals(0.2, testDocument.termFrequency("Η".toLowerCase()));
    assertEquals(0.4, testDocument.termFrequencyFullSearch("του".toLowerCase()));
    assertEquals(testDocument.termFrequency("του"), testDocument.termFrequencyFullSearch("του".toLowerCase()));
  }
}
