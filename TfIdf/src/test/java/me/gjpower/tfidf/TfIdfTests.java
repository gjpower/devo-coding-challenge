package me.gjpower.tfidf;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import org.junit.jupiter.api.Test;

public class TfIdfTests  {

  @Test
  void checkFullCalculation() throws IOException {
    TfIdf moonTerm = new TfIdf("moon");
    TfIdf catTerm = new TfIdf("cat");

    final String moonCow = "Hey, diddle, diddle,\n"
        + "The cat and the fiddle,\n"
        + "The cow jumped over the moon;\n"
        + "The little dog laughed\n"
        + "To see such fun,\n"
        + "And the dish ran away with the spoon.";
    final String dingDong = "Ding, dong, bell,\n"
        + "Pussy’s in the well.\n"
        + "Who put her in?\n"
        + "Little Johnny Thin.\n"
        + "Who pulled her out?\n"
        + "Little Tommy Stout.\n"
        + "What a naughty boy was that,\n"
        + "To drown a pussy cat?\n"
        + "Who never did any harm?\n"
        + "But scared all the mice\n"
        + "In the Farmer's barn.";


    Document moonCowDoc = new Document(new BufferedReader(new StringReader(moonCow)), "moonCow");
    Document dingDongDoc = new Document(new BufferedReader(new StringReader(dingDong)), "dingDong");

    moonTerm.updateTermFrequency(moonCowDoc.filename, moonCowDoc);
    moonTerm.updateTermFrequency(dingDongDoc.filename, dingDongDoc);

    catTerm.updateTermFrequency(moonCowDoc.filename, moonCowDoc);
    catTerm.updateTermFrequency(dingDongDoc.filename, dingDongDoc);

    Map<String, Double> moonTFidfs = moonTerm.updateTfIdfs();
    Map<String, Double> catTFidfs = catTerm.updateTfIdfs();

    assertEquals(0.0, catTFidfs.get(moonCowDoc.filename));
    assertEquals(0.0, catTFidfs.get(dingDongDoc.filename));

    assertEquals(0.010034333188799373, moonTFidfs.get(moonCowDoc.filename));
    assertEquals(0.0, moonTFidfs.get(dingDongDoc.filename));
  }

}
