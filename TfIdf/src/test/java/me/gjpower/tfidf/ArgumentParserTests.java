package me.gjpower.tfidf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ArgumentParserTests {

  @Test
  void findExpectedArguments() {
    String[] arguments = {"-d", "hello", "-w", "world"};
    String[] expectedFlags = {"-d", "-w"};
    ArgumentParser parser = new ArgumentParser(arguments);

    assertEquals(2, parser.flagValues.size());
    assertTrue(parser.checkExpectedFlags(expectedFlags));
    assertEquals("hello", parser.flagValues.get("-d"));
    assertEquals("world", parser.flagValues.get("-w"));
  }

}
