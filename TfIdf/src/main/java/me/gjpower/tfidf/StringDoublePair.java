package me.gjpower.tfidf;

// helper class to store and sort filename tfidf pairs
public class StringDoublePair implements Comparable<StringDoublePair> {
  String aString;
  double aDouble;

  public StringDoublePair(String aString, double aDouble) {
    this.aString = aString;
    this.aDouble = aDouble;
  }

  // implementing compare to use List.sort method
  public int compareTo(StringDoublePair other) {
    if (this.aDouble == other.aDouble) {
      return 0;
    }

    if (this.aDouble - other.aDouble > 0.0) {
      return 1;
    }
    return -1;
  }

  public String toString() {
    return "String: " + aString + ", Double: " + aDouble;
  }
}
