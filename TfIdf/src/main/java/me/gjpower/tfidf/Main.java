package me.gjpower.tfidf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Main {

  // will return false if no new files have been added
  // otherwise will parse file to document and update term frequencies for any files added
  // and return true to notify new files and recalculation needed
  public static boolean updateFiles(
      List<TfIdf> tfIdfs, File[] files, Map<String, Document> documents, Pattern splitOnMatch) {
    boolean newFilesExist = false;
    // if no files return false
    if (files == null) {
      return false;
    }

    for (File file : files) {
      // if this is a new file
      if (!documents.containsKey(file.getPath())) {
        newFilesExist = true;

        try {
          BufferedReader fileReader = new BufferedReader(new FileReader(file));
          Document currentDoc = new Document(fileReader, file.getName(), splitOnMatch);
          documents.put(file.getPath(), currentDoc);

          // generate termFrequencies for every term for this new document
          for (TfIdf currentTerm : tfIdfs) {
            currentTerm.updateTermFrequency(file.getPath(), currentDoc);
          }
        } catch (IOException exception) {
          System.err.printf(
              "Error reading file %s due to %s%n", files[0].getPath(), exception.getMessage());
        }
      }
    }

    return newFilesExist;
  }

  // clear per file TfIDF totals back to zero before summing
  public static void resetFileTotalIdfs(
      Map<String, Double> fileSummedTfIdfs, Map<String, Document> documents) {
    for (String filename : documents.keySet()) {
      fileSummedTfIdfs.put(filename, 0.0);
    }
  }

  // recalculate TfIDF for each file and sum them for per file totals
  public static void recalculateTfIdfs(
      List<TfIdf> termsTfIdfs, Map<String, Double> fileSummedTfIdfs) {
    for (TfIdf currentTerm : termsTfIdfs) {
      Map<String, Double> tfIdfs = currentTerm.updateTfIdfs();

      for (Map.Entry<String, Double> fileTfIdf : tfIdfs.entrySet()) {
        String filename = fileTfIdf.getKey();
        double value = fileTfIdf.getValue();
        double accFileTfIdf = fileSummedTfIdfs.get(filename);
        accFileTfIdf += value;
        fileSummedTfIdfs.put(filename, accFileTfIdf);
      }
    }
  }

  // copy per file TfIDFs to arraylist to sort and then rank, returning the ranked list
  // A binary search tree could also be implemented and used to avoid sorting
  // although element insert and lookup will have greater complexity than O(1) for the sorted list
  public static List<StringDoublePair> rankTfIdfs(Map<String, Double> fileSummedTfIdfs) {
    List<StringDoublePair> fileRankingTfIdfs = new ArrayList<>();
    // add all filenames and TfIdfs to list
    for (Map.Entry<String, Double> entry : fileSummedTfIdfs.entrySet()) {
      String filename = entry.getKey();
      double tfIdf = entry.getValue();

      fileRankingTfIdfs.add(new StringDoublePair(filename, tfIdf));
    }

    // sort the list, high to low
    fileRankingTfIdfs.sort(Collections.reverseOrder());

    return fileRankingTfIdfs;
  }

  public static void main(String[] args) throws InterruptedException {
    // split on any non word characters
    // (including apostrophes and right quotation for Latin alphabet languages with contractions)
    final Pattern splitOnMatch = Pattern.compile("[^\\p{L}\\p{M}\\p{N}'\u02bc\u2019]+");
    final int topResultsAmount;
    final long sleepPeriod;

    // Parse all arguments
    ArgumentParser argParse = new ArgumentParser(args);

    // ensure all expected flags are supplied
    final String[] expectedFlags = {"-d", "-n", "-p", "-t"};
    if (!argParse.checkExpectedFlags(expectedFlags)) {
      System.err.println(
          "Missing arguments: expected -d <directory> -n <top_results_count> -p <refresh_period> -t <terms>");
      return;
    }

    final String directoryName = argParse.flagValues.get("-d");
    try {
      topResultsAmount = Integer.parseInt(argParse.flagValues.get("-n").trim());
      sleepPeriod = Integer.parseInt(argParse.flagValues.get("-p").trim()) * 1000L;
    } catch (NumberFormatException exception) {
      System.err.println(
          "Error parsing arguments, -n <top_results_count> and -p <refresh_period> must be integers");
      return;
    }
    final String[] terms = splitOnMatch.split(argParse.flagValues.get("-t"));

    // Arguments parsed now initialise main data structures
    List<TfIdf> termsTfIdfs = new ArrayList<>();
    Map<String, Document> documents = new HashMap<>();
    Map<String, Double> fileSummedTfIdfs = new HashMap<>();

    // initialise terms
    // make the terms lowercase as we are only interested in terms, not capitalisation
    for (String term : terms) {
      termsTfIdfs.add(new TfIdf(term.toLowerCase()));
    }

    File dir = new File(directoryName);

    while (true) {
      File[] files = dir.listFiles();

      // handle any new files then update and display Tf-Idfs if any new files exist
      if (updateFiles(termsTfIdfs, files, documents, splitOnMatch)) {
        // reset per file tfIdf to 0 to prepare to sum
        resetFileTotalIdfs(fileSummedTfIdfs, documents);
        // recalculate tfIdf for each term and sum tfIdf for each file
        recalculateTfIdfs(termsTfIdfs, fileSummedTfIdfs);
        // place results into arraylist and sort
        List<StringDoublePair> fileRankingTfIdfs = rankTfIdfs(fileSummedTfIdfs);

        System.out.println("=============== Top Results ===============");
        // print the ranked values
        int nResults = Math.min(topResultsAmount, fileRankingTfIdfs.size());
        for (int i = 0; i < nResults; i++) {
          StringDoublePair pair = fileRankingTfIdfs.get(i);
          System.out.printf("File: %s, tfidf: %f%n", pair.aString, pair.aDouble);
        }
        System.out.println();
      }

      Thread.sleep(sleepPeriod);
    }
  }
}
