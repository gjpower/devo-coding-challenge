package me.gjpower.tfidf;

import java.util.HashMap;
import java.util.Map;

// very basic argument parser creating flag -> value pairs
// flags must begin with '-' and be followed by value
public class ArgumentParser {
  Map<String, String> flagValues;

  public ArgumentParser(String[] args) {
    flagValues = new HashMap<>();

    for (int i = 0; i < args.length; i++) {
      String arg = args[i];
      if (arg.length() > 0
          && arg.charAt(0) == '-'
          && i < args.length - 1
          && !flagValues.containsKey(arg)) {
        flagValues.put(arg, args[i + 1]);
        // handling argument value pair so double increment
        i++;
      }
    }
  }

  public boolean checkExpectedFlags(String[] flags) {
    for (String flag : flags) {
      if (!flagValues.containsKey(flag)) {
        return false;
      }
    }

    return true;
  }
}
