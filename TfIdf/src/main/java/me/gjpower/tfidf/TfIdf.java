package me.gjpower.tfidf;

import java.util.HashMap;
import java.util.Map;

public class TfIdf {
  String term;
  int appearances;
  double idf;
  Map<String, Double> frequencies;
  Map<String, Double> tfIdfs;

  public TfIdf(String term) {
    this.term = term;
    appearances = 0;
    idf = 0.0;
    frequencies = new HashMap<>();
    tfIdfs = new HashMap<>();
  }

  // fetch term frequency and increment document count if greater than 0
  // otherwise return the already calculated value
  public double updateTermFrequency(String filename, Document doc) {
    double termFreq;

    if (!frequencies.containsKey(filename)) {
      termFreq = doc.termFrequency(term);
      frequencies.put(filename, termFreq);

      if (termFreq > 0.0) {
        appearances++;
      }
    } else {
      termFreq = frequencies.get(filename);
    }

    return termFreq;
  }

  // recalculate IDF for term
  public double inverseDocumentFrequency() {
    if (appearances == 0) {
      idf = 0.0;
    } else {
      idf = Math.log10((double) frequencies.size() / (double) (appearances));
    }
    return idf;
  }

  // first recalulate IDF for term, then multiply TF * IDF to generate TfIDF for each document
  public Map<String, Double> updateTfIdfs() {
    inverseDocumentFrequency();

    for (Map.Entry<String, Double> entry : frequencies.entrySet()) {
      String filename = entry.getKey();
      double tf = entry.getValue();

      tfIdfs.put(filename, tf * idf);
    }

    return tfIdfs;
  }

  public Map<String, Double> getTfIdfs() {
    return tfIdfs;
  }

  public Map<String, Double> getTfs() {
    return frequencies;
  }

  public String getTerm() {
    return term;
  }
}
