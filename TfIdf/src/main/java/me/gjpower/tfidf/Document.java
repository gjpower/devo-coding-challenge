package me.gjpower.tfidf;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Document {
  String text;
  String filename;
  Map<String, Integer> documentTermMap;
  Integer wordCount;

  Document(BufferedReader textSource, String filename) throws IOException {
    this(textSource, filename, Pattern.compile("[^\\p{L}\\p{M}\\p{N}'\u02bc\u2019]+"));
  }

  // build a hashmap with the count of occurrences of every term in the document
  Document(BufferedReader textSource, String filename, Pattern splitMatch) throws IOException {
    this.filename = filename;
    documentTermMap = new HashMap<>();
    wordCount = 0;

    StringBuilder sourceBufferer = new StringBuilder();
    String sourceBuffer;
    while ((sourceBuffer = textSource.readLine()) != null) {
      // we are interested in terms not letter case so convert all to lower case
      sourceBuffer = sourceBuffer.toLowerCase();
      String[] words = splitMatch.split(sourceBuffer);
      wordCount += words.length;

      for (String word : words) {
        Integer currentWordCount = 1;
        if (documentTermMap.containsKey(word)) {
          currentWordCount = documentTermMap.get(word);
          currentWordCount++;
        }
        documentTermMap.put(word, currentWordCount);
      }

      sourceBufferer.append(sourceBuffer);
      sourceBufferer.append("\n");
    }

    text = sourceBufferer.toString();
  }

  // once the document has been parsed with terms to a hash table this is just a simple hash lookup
  double termFrequency(String term) {
    if (documentTermMap.containsKey(term)) {
      return (double) documentTermMap.get(term) / (double) wordCount;
    }

    return 0.0;
  }

  // these will do a full search of the original document instead of a simple hash lookup
  double termFrequencyFullSearch(String term) {
    Pattern termPattern = Pattern.compile(term + "\\W");
    return termFrequencyFullSearch(termPattern);
  }

  double termFrequencyFullSearch(Pattern termPattern) {
    int occurrences = 0;

    Matcher termMatcher = termPattern.matcher(text);

    while (termMatcher.find()) occurrences++;

    return (double) occurrences / (double) wordCount;
  }
}
