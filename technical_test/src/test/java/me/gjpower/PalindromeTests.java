package me.gjpower;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PalindromeTests {

  @Test
  void twoLetter() {
    assertTrue(Palindrome.palindrome("BB"), "BB is a palindrome");
  }

  @Test
  void emptyIsNotPalindrome() {
    assertFalse(Palindrome.palindrome(""), "An empty string is not a palindrome");
  }

  @Test
  void oneLetterIsNotPalindrome() {
    assertFalse(Palindrome.palindrome("a"), "A single letter is not a palindrome");
  }

  @Test
  void checkUTF8Compatible() {
    assertTrue(Palindrome.palindrome("πΩΩΩπ"));
    assertTrue(Palindrome.palindrome("πΩΩπ"));
    assertFalse(Palindrome.palindrome("πΩkπ"));
  }

  @Test
  void checkNumeric() {
    assertTrue(Palindrome.palindrome("122333221"));
    assertFalse(Palindrome.palindrome("123456"));
  }

  @Test
  void checkOddAndEvenLength() {
    assertTrue(Palindrome.palindrome("tattarrattat"));
    assertTrue(Palindrome.palindrome("tattarattat"));
  }
}
