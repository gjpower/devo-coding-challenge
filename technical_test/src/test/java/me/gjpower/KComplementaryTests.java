package me.gjpower;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Comparator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

public class KComplementaryTests {
  static Integer k = 8;
  static Integer[] searchValues = {-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  static List<Integer[]> expectedPairs;

  // comparator implemented to sort pairs for the purpose of testing
  // sort by i
  Comparator<Integer[]> pairComparator =
      new Comparator<Integer[]>() {
        @Override
        public int compare(Integer[] e1, Integer[] e2) {
          return e1[0].compareTo(e2[0]);
        }
      };

  @BeforeAll
  static void setUpExpectedPairs() {
    expectedPairs = new ArrayList<>();
    expectedPairs.add(new Integer[]{1, 13});
    expectedPairs.add(new Integer[]{2, 12});
    expectedPairs.add(new Integer[]{3, 11});
    expectedPairs.add(new Integer[]{4, 10});
    expectedPairs.add(new Integer[]{5, 9});
    expectedPairs.add(new Integer[]{6, 8});
    expectedPairs.add(new Integer[]{8, 6});
    expectedPairs.add(new Integer[]{9, 5});
    expectedPairs.add(new Integer[]{10, 4});
    expectedPairs.add(new Integer[]{11, 3});
    expectedPairs.add(new Integer[]{12, 2});
    expectedPairs.add(new Integer[]{13, 1});
  }

  @Test
  void findK8Optimal() {
    List<Integer[]> pairs = KComplementary.optimised(k, searchValues);
    pairs.sort(pairComparator);

    assertEquals(expectedPairs.size(), pairs.size());
    for (int i = 0; i < pairs.size(); i++) {
      Integer[] expectedPair = expectedPairs.get(i);
      Integer[] foundPair = pairs.get(i);
      assertArrayEquals(expectedPair, foundPair);
    }
  }

  @Test
  void findK8Brute() {
    List<Integer[]> pairs = KComplementary.brute(k, searchValues);
    pairs.sort(pairComparator);

    assertEquals(expectedPairs.size(), pairs.size());
    for (int i = 0; i < pairs.size(); i++) {
      Integer[] expectedPair = expectedPairs.get(i);
      Integer[] foundPair = pairs.get(i);
      assertArrayEquals(expectedPair, foundPair);
    }
  }
}
