package me.gjpower;

import java.util.*;

public class KComplementary {

  public static List<Integer[]> brute(Integer k, Integer[] a) {
    ArrayList<Integer[]> pairs = new ArrayList<>();

    for (int i = 0; i < a.length; i++) {
      for (int j = i + 1; j < a.length; j++) {
        if (a[i] + a[j] == k) {
          pairs.add(new Integer[] {i, j});
          pairs.add(new Integer[] {j, i});
        }
      }
    }

    return pairs;
  }

  public static List<Integer[]> optimised(Integer k, Integer[] a) {
    ArrayList<Integer[]> pairs = new ArrayList<>();

    // create unique set with positions of those occurrences
    // that is, 3: [0, 1, 2], 4: [5], 6: [7, 8, 9]
    HashMap<Integer, ArrayList<Integer>> distinctValues = mapDistinctValuesAtIndices(a);

    Set<Integer> alreadyUsed = new HashSet<>();

    for (Map.Entry<Integer, ArrayList<Integer>> entry : distinctValues.entrySet()) {
      Integer value = entry.getKey();
      // if this value already appeared as a target skip it
      if (alreadyUsed.contains(value)) {
        continue;
      }
      Integer target = k - value;

      if (value.equals(target)) {
        ArrayList<Integer> indexList = entry.getValue();
        // generate combinations where the target value matches the current value
        addSelfPairs(indexList, pairs);
      } else if (distinctValues.containsKey(target)) {
        ArrayList<Integer> iList = entry.getValue();
        ArrayList<Integer> jList = distinctValues.get(target);
        // generate combinations where the target is different than the source
        addDistinctPairs(iList, jList, pairs);
        // mark the target as already used so it is skipped and not repeated
        alreadyUsed.add(target);
      }
    }

    return pairs;
  }

  private static HashMap<Integer, ArrayList<Integer>> mapDistinctValuesAtIndices(Integer[] a) {
    HashMap<Integer, ArrayList<Integer>> distinctValues = new HashMap<>();

    for (int i = 0; i < a.length; i++) {
      int value = a[i];

      if (distinctValues.containsKey(value)) {
        distinctValues.get(value).add(i);
      } else {
        ArrayList<Integer> valueAtIndex = new ArrayList<>();
        valueAtIndex.add(i);
        distinctValues.put(value, valueAtIndex);
      }
    }

    return distinctValues;
  }

  private static void addSelfPairs(ArrayList<Integer> indexList, ArrayList<Integer[]> pairs) {
    for (int i = 0; i < indexList.size(); i++) {
      for (int j = i + 1; j < indexList.size(); j++) {
        int iValue = indexList.get(i);
        int jValue = indexList.get(j);
        // add matched pair (i, j)
        pairs.add(new Integer[]{iValue, jValue});
        // add its reflection (j, i)
        pairs.add(new Integer[]{jValue, iValue});
      }
    }
  }

  private static void addDistinctPairs(
      ArrayList<Integer> iList, ArrayList<Integer> jList, ArrayList<Integer[]> pairs) {
    for (Integer iValue : iList) {
      for (Integer jValue : jList) {
        // add matched pair (i, j)
        pairs.add(new Integer[]{iValue, jValue});
        // add its reflection (j, i)
        pairs.add(new Integer[]{jValue, iValue});
      }
    }
  }
}
