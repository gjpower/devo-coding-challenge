package me.gjpower;

import java.util.*;

public class Main {

  public static void main(String[] args) {
    String oneLetter = "a";
    String twoLetter = "bb";
    String threeLetter = "ccc";
    String doorKnock = "tattarrattat";
    String doorKnockShort = "tattarattat";
    String numbersY = "122333221";
    String numbersN = "123456";
    String pi = "πΩπ";
    String pi2 = "πΩΩπ";
    String pi3 = "πΩkπ";
    String empty = "";

    String[] toTest = {
      oneLetter,
      twoLetter,
      threeLetter,
      doorKnock,
      doorKnockShort,
      numbersY,
      numbersN,
      pi,
      pi2,
      pi3,
      empty
    };

    for (String testWord : toTest) {
      System.out.printf("%s is a palindrome? %b%n", testWord, Palindrome.palindrome(testWord));
    }

    Random r = new Random();

    ArrayList<Integer> testArrayList = new ArrayList<>();

    // generate random large list for relative performance test
    for (int i = 0; i < 10000; i++) {
      testArrayList.add(r.nextInt(100));
    }

    Integer k = 16;
    //        Integer[] testArray = {1, 2, 3, 4, 5, 6, 7, 8, 8, 8, 6, 4, 9, 10, 11, 12, 13, 14, 15,
    // 16};
    Integer[] testArray = testArrayList.toArray(new Integer[testArrayList.size()]);

    long start = System.currentTimeMillis();
    List<Integer[]> result = KComplementary.optimised(k, testArray);
    long end = System.currentTimeMillis();

    // basic performance comparison of k-complementary implementations
    System.out.printf("K_complementary optimised: %d millis%n", end - start);
    System.out.println(result.size());

    System.out.println("\n================ BRUTE ================");
    start = System.currentTimeMillis();
    List<Integer[]> result2 = KComplementary.brute(k, testArray);
    end = System.currentTimeMillis();

    System.out.printf("K_complementary brute: %d millis%n", end - start);
    System.out.println(result2.size());
  }
}
