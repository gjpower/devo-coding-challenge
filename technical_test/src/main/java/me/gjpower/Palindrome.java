package me.gjpower;

public class Palindrome {

  public static boolean palindrome(String word) {
    // depending on definition of a palindrome it must have a minimum number of letters
    // an empty string isn't a palindrome
    // a single character can be a word but not a palindrome
    // this is subjective and this if statement can be removed if single letters and empty strings
    // may be considered palindromes
    if (word.length() < 2) {
      return false;
    }

    // check first and last letters are the same until the middle is reached
    // if any mirrored pair doesn't match it is not a palindrome
    // if they all match it is
    for (int i = 0, j = word.length() - 1; i < word.length() / 2; i++, j--) {
      if (word.charAt(i) != word.charAt(j)) {
        return false;
        }
    }

    return true;
  }
}
